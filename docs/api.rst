.. _api:

API
===

Evolutionary tracks
-------------------

.. autofunction:: mistery.get_tracks
.. autofunction:: mistery.get_track

Isochrones
----------

.. autofunction:: mistery.get_isochrones
.. autofunction:: mistery.get_isochrone

.. _tables:

Photometry table options
------------------------

Scraped from MIST form.

=================  =========
Option             Passbands
=================  =========
``'CFHTugriz'``    CFHT/MegaCam
``'DECam'``        DECam
``'HST_ACSHR'``    HST ACS/HRC
``'HST_ACSWF'``    HST ACS/WFC
``'HST_WFC3'``     HST WFC3/UVIS+IR
``'HST_WFPC2'``    HST WFPC2
``'GALEX'``        GALEX
``'JWST'``         JWST
``'LSST'``         LSST
``'PanSTARRS'``    PanSTARRS
``'SDSSugriz'``    SDSS
``'SkyMapper'``    SkyMapper
``'SPITZER'``      Spitzer IRAC
``'SPLUS'``        S-PLUS
``'HSC'``          Subaru Hyper Suprime-Cam
``'IPHAS'``        INT / IPHAS
``'Swift'``        Swift
``'UBVRIplus'``    UBV(RI)c + 2MASS + Kepler + Hipparcos + Gaia (DR2/MAW/EDR3) + TESS
``'UKIDSS'``       UKIDSS
``'UVIT'``         UVIT
``'VISTA'``        VISTA
``'WashDDOuvby'``  Washington + Strömgren + DDO51
``'WFIRST'``       WFIRST (preliminary)
``'WISE'``         WISE
=================  =========
