# mistery

*mistery* (c.f. "MIST query") is a Python module for retrieving data
from the [MESA Isochrones and Stellar
Tracks](http://waps.cfa.harvard.edu/MIST/) (MIST) database of stellar
models.  The module allows you to fetch data by submitting requests to
the web interpolator.  It is intended as a lightweight teaching tool
that lets students access real-world stellar model data with
minimal effort.  If you're
doing intensive scientific work, you may prefer to access
the data in other ways (e.g. by downloading [packaged model
grids](http://waps.cfa.harvard.edu/MIST/model_grids.html)).

See [the documentation](https://warrickball.gitlab.io/mistery) for more
information.
